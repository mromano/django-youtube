from django.db import models

# Create your models here.

class Video(models.Model):
    id = models.CharField(max_length=12, primary_key=True)
    title = models.CharField(max_length=256)
    link = models.CharField(max_length=256)
    description = models.CharField(max_length=256)
    date = models.CharField(max_length=256)
    image = models.CharField(max_length=256)
    channel = models.CharField(max_length=256)
    urlChannel = models.CharField(max_length=256)

    selected = models.BooleanField(default=False)
