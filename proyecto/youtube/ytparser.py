
from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import sys
import string


class YTHandler(ContentHandler):
    """Class to handle events fired by the SAX parser
    Fills in self.videos with title, link and id for videos
    in a YT channel XML document.
    """

    def __init__ (self):
        """Initialization of variables for the parser
        * inEntry: within <entry>
        * inContent: reading interesting target content (leaf strings)
        * content: target content being readed
        * title: title of the current entry
        * id: id of the current entry
        * link: link of the current entry
        * videos: list of videos (<entry> elements) in the channel,
            each video is a dictionary (title, link, id)
        """
        self.inEntry = False
        self.inContent = False
        self.content = ""
        self.title = ""
        self.id = ""
        self.link = ""
        #nuevos
        self.channel = ""
        self.urlChannel = ""
        self.image = ""
        self.description = ""
        self.date = ""

        self.videos = []


    def startElement (self, name, attrs):
        if name == 'entry':
            self.inEntry = True
        elif self.inEntry:
            if name == 'title':
                self.inContent = True
            elif name == 'link':
                self.link = attrs.get('href')
            elif name == 'yt:videoId':
                self.inContent = True
            #diferencia: una está dentro del elemento (<media:thumbnail [...]>)
            #y otra anuncia que empieza el elemento (<media:description> [...])
            elif name == "media:thumbnail":
                self.image = attrs.get('url')
            elif name == "media:description":
                self.inContent = True
            #url del canal
            elif name == "uri":
                self.inContent = True
            #nombre del canal
            elif name == "name":
                self.inContent = True
            elif name == "published":
                self.inContent = True

    def endElement (self, name):
        global videos

        if name == 'entry':
            self.inEntry = False
            self.videos.append({'link': self.link,
                                'title': self.title,
                                'id': self.id,
                                'description': self.description,
                                'date': self.date,
                                'image': self.image,
                                'nombreCanal': self.channel,
                                'urlCanal': self.urlChannel})
            #print(self.videos)

        elif self.inEntry:
            if name == 'title':
                self.title = self.content

            elif name == 'yt:videoId':
                self.id = self.content

            elif name == "media:description":
                self.description = self.content

            elif name == "uri":
                self.urlChannel = self.content

            elif name == "name":
                self.channel = self.content

            elif name == "published":
                self.date = self.content

            #lo pongo al final porque aparecía en todas
            self.content = ""
            self.inContent = False

    def characters (self, chars):
        if self.inContent:
            self.content = self.content + chars


class YTChannel:
    #esto es siempre igual
    def __init__(self, stream):
        self.parser = make_parser()
        self.handler = YTHandler()
        self.parser.setContentHandler(self.handler)
        self.parser.parse(stream)

    def videos (self):
        return self.handler.videos


# Load parser and driver
#Parser = make_parser()
#Parser.setContentHandler(YTHandler())

#if __name__ == "__main__":

    #if len(sys.argv)<2:
        #print("Usage: python xml-parser-youtube.py <document>")
        #print()
        #print(" <document>: file name of the document to parse")
        #sys.exit(1)

    # Ready, set, go!
    #xmlFile = open(sys.argv[1],"r")

    #Parser.parse(xmlFile)
