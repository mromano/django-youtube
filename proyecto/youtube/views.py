from django.shortcuts import render
from django.http import HttpResponse
from .models import Video

from django.views.decorators.csrf import csrf_protect

#página principal:
#lista de vídeos (seleccionados y seleccionables)
@csrf_protect
def index(request):
    #return HttpResponse("Index")
    #parte del formulario
    if request.method=="POST":
        action = request.POST['action']
        print("action: " + action)
        chosen = request.POST['id']
        print("chosen: " + chosen)
        c = Video.objects.get(id=chosen)
        #cambio el flag
        if action == "Select":
            print("Entró a select")
            c.selected = True
        elif action == "Deselect":
            c.selected = False
        else:
            print("Entró a error")
            return HttpResponse("Error")
        c.save()

    #parte que se ejecuta siempre
    #csrf_token = get_token(request)
    context = {'content_list': Video.objects.all()}
    return render(request,'youtube/index.html', context)


#página de vídeo:
#toda la info del vídeo que hay en "Video"
def video(request, id):
    #return HttpResponse("Video")
    #saco el vídeo de la base de datos
    c = Video.objects.get(id=id)

    #saco toda la info que quiero meter en el template del video
    title = c.title
    link = c.link
    description = c.description
    date = c.date
    image = c.image
    channel = c.channel
    urlChannel = c.urlChannel

    #print(urlChannel)

    context ={"title": title, "link": link, "description": description,
            "date": date, "image": image, "channel": channel, "urlChannel": urlChannel}

    return render(request,'youtube/video.html', context)
