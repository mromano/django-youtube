"""Youtube app resource configuration
"""
from django.urls import path
from . import views

urlpatterns = [
    path('youtube/', views.index, name='index'),
    path('youtube/<str:id>', views.video, name='video'),
]
